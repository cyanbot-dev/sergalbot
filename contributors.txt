Contributors for the mainline code (Node.js):
CyanEve
dogesoft
narwhalpotatoman "Hiro"

Contributors for the Python port:
Ki2ne
Arokh Rimewing

Contributors for writing:
HowlingWolven "Shira"
