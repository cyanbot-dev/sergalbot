#!/usr/bin/bash

# Sergalbot startup script for Linux. Default settings for Ubuntu (gnome-terminal).
# Untested/experimental until further notice. Use at your own risk!

# Changes title.
echo -ne "\033]0;Moody Sergal bot\007"

# Gets original profile colors. 
o_fg=$(gconftool-2 --get "/apps/gnome-terminal/profiles/Default/foreground_color")
o_bg=$(gconftool-2 --get "/apps/gnome-terminal/profiles/Default/background_color")

# Sets new profile colors.
gconftool-2 --set "/apps/gnome-terminal/profiles/<profile_name>/foreground_color" --type string "#FFFFFF"
gconftool-2 --set "/apps/gnome-terminal/profiles/<profile_name>/background_color" --type string "#0000FF"

cd "bot directory"
while true; do # Since there's no GOTO statement in Bash, a possible option is to use a infinite loop. 
    echo "Starting Bot..."
    node ./testbot.js # Assuming that the bot script is in current directory
    read -n1 -r -p "Press any key to continue..." key # equivalent of pause
    clear
    echo "Restarting bot..."
done
